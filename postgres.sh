#!/bin/bash

read -p "Email: " EMAIL
read -p "Password: " -s PASSWORD

# postgres
apt -y update
apt install -y curl gnupg2 postgresql postgresql-contrib
sudo -u postgres psql postgres -c "alter role postgres with password '$PASSWORD'"

curl https://gitlab.com/xtec/daw-2-data/-/raw/main/scott.sql | sudo -u postgres psql
# curl https://raw.githubusercontent.com/lamerce/postgres-data/main/dvd_rental.dump  | sudo -u postgres pg_restore -C -d postgres
# curl https://raw.githubusercontent.com/lamerce/postgres-data/main/airline_flights.dump | sudo -u postg res pg_restore -C -d postgres
# curl https://raw.githubusercontent.com/lamerce/postgres-data/main/adventure_works.dump | sudo -u postg res pg_restore -C -d postgres
# curl https://raw.githubusercontent.com/lamerce/postgres-data/main/omdb.dump | sudo -u postgres pg_restore -C -d postgres


# pgAdmin
curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add
sh -c 'echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$(lsb_release -cs) pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
apt install -y pgadmin4-web
export PGADMIN_SETUP_EMAIL="$EMAIL"
export PGADMIN_SETUP_PASSWORD="$PASSWORD"
. /usr/pgadmin4/bin/setup-web.sh --yes
